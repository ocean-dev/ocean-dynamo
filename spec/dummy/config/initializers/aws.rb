# DynamoDB and general client init -----------------------------------------
cfg = {
  region: "eu-west-1",    # Doesn't matter in this context
  credentials: Aws::Credentials.new('not', 'used'),
  endpoint: 'http://localhost:4569'
}
Aws.config.update(cfg)
